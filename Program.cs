﻿using System;
using System.Collections.Generic;

namespace Biblioteka
{
    class Program
    {
        static void Main(string[] args)
        {
            var booi = new List<book>() { };
            var booi_fab = new List<book_Fab>() { };
            var magazine = new List<magazine>() { };
            var magazine_bike = new List<magazine_bikers>() { };
            string a = "";
            int i = -1;
            while (a != "5")
            {
                a = "";
                string b = "";
                string c = "";
                var ta = new List<string> {"1", "2", "3", "4", "5"};
                while (!ta.Contains(a))
                {
                    Console.WriteLine("do u wanna add a book or magazine chose option 1 ");
                    Console.WriteLine("do u wanna show all books chose option 2 ");
                    Console.WriteLine("do u wanna show all magazines 3");
                    Console.WriteLine("do u wanna show books and magazines 4)");
                    Console.WriteLine("do u wanna end task chose 5");
                    a = Console.ReadLine();
                }

                if (a == "1")
                {
                    while (b is not ("1" or "2"))
                    {
                        Console.WriteLine("do u wanna add book chose 1 or magazine 2 ?");
                        b = Console.ReadLine();
                    }
                    if (b == "1")
                    {
                        
                        while (c is not ("1" or "2"))
                        {
                            Console.WriteLine("do u wanna add book  1 or fabular book 2?");
                            c = Console.ReadLine();
                        }

                        if (c == "1")
                        {
                            Console.WriteLine("give issue number:");
                            var n = Console.ReadLine();
                            Console.WriteLine("give title:");
                            var t = Console.ReadLine();
                            Console.WriteLine("give autor's name:");
                            var y = Console.ReadLine();
                            booi.Add(new book() {number = n, title = t, autor = y}); 
                        }

                        if (c == "2")
                        {
                            Console.WriteLine("give issue number:");
                            var n = Console.ReadLine();
                            Console.WriteLine("give title:");
                            var t = Console.ReadLine();
                            Console.WriteLine("give autor's name:");
                            var y = Console.ReadLine();
                            booi_fab.Add(new book_Fab() {number = n, title = t, autor = y}); 
                        }
                    }
                    if (b == "2")
                    {
                        while (c is not ("1" or "2"))
                        {
                            Console.WriteLine("do u wanna add  magazine 1 or moto magazine  2 ?");
                            c = Console.ReadLine();
                        }

                        if (c == "1")
                        {
                            Console.WriteLine("give prod's name:");
                            var p = Console.ReadLine();
                            Console.WriteLine("give year :");
                            var r = Console.ReadLine();
                            Console.WriteLine("give title:");
                            var t = Console.ReadLine();
                            magazine.Add(new magazine() {prod = p, year = r, title = t}); 
                        }
                        if (c == "2")
                        {
                            Console.WriteLine("give producents name:");
                            var p = Console.ReadLine();
                            Console.WriteLine("give year:");
                            var r = Console.ReadLine();
                            Console.WriteLine("give title:");
                            var t = Console.ReadLine();
                            magazine_bike.Add(new magazine_bikers() {prod = p, year = r, title = t}); 
                        }
                    }
                }

                if (a == "2")
                {
                    foreach (var x in booi)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in booi_fab)
                    {
                        Console.WriteLine(x.ToString());
                    }
                }
                
                if (a == "3")
                {
                    foreach (var x in magazine)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in magazine_bike)
                    {
                        Console.WriteLine(x.ToString());
                    }
                }

                if (a == "4")
                {
                    foreach (var x in booi)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in booi_fab)
                    {
                        Console.WriteLine(x.ToString());
                    }
                    
                    foreach (var x in magazine)
                    {
                        Console.WriteLine(x.ToString());
                    }

                    foreach (var x in magazine_bike)
                    {
                        Console.WriteLine(x.ToString());
                    }
                }
            }
        }
    }
    class book
    {
        internal string number;
        internal string title;
        internal string autor;

        public void ustaw_nr(string nr)
        {
            number = nr;
        }

        public void ustaw_tytuł(string ty)
        {
            title = ty;
        }

        public void ustaw_autor(string at)
        {
            autor = at;
        }
        public override string ToString()
        {
            return "number: " + number.ToString() + " title: " + title.ToString() + " Autor: " + autor.ToString();
        }
    }
    class book_Fab : book
    {
        private string g = "Fabular book ";
    }
    class magazine
    {
        internal string prod;
        internal string year;
        internal string title;
        public void ustaw_producent(string at)
        {
            prod = at;
        }
        public void ustaw_rok(string nr)
        {
            year = nr;
        }
        public void ustaw_tytuł(string ty)
        {
            title = ty;
        }
        public override string ToString()
        {
            return "prod: " + prod.ToString() + "year: " + year.ToString() + " title: " + title.ToString();
        }
    }
    class magazine_bikers : magazine
    {
        private string g = "bike magazine ";
    }
}
